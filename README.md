# EarthCare: Sky

This library loads and manages data representing a clear/cloudy sky.
The atmospheric gas mixture is loaded from a
[HTGOP](https://gitlab.com/meso-star/htgop) file while cloud and
aerosol properties are loaded from data stored with respect to the
[ECRP](https://gitlab.com/najdaVLF/ecrp) fileformat. Once provided,
the clouds can be repeated infinitely into the X and Y dimension.

ECSky relies onto the [Star-VX](https://gitlab.com/meso-star/star-vx) library
to build space partitioning data structures upon raw sky data. These structures
can then be used in conjunction of null-collision algorithms to accelerate the
tracking of a ray into this inhomogeneous medium, as described in [Villefranque
et al.
(2019)](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2018MS001602).
These accelerating structures are built with respect to an optical thickness
criterion whose threshold is user defined. One can also fix the maximum
resolution of the structures in order to constraint their memory consumption.
Even though the building itself of the structures is quite efficient, computing
their underlying data from the input files can be time consuming. So, once
built, these structures can be stored into a file to drastically speed up the
subsequent initialisation steps of the same sky. We point out that this file is
valid as long as the provided HTGOP, HTCP and HTMie files are the ones used to
build the cached structures. If not, an error is returned on sky creation.

## How to build

This library is compatible GNU/Linux 64-bits. It relies on the
[CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build. 
It also depends on the
[ECRP](https://gitlab.com/najdaVLF/ecrp/),
[RSys](https://gitlab.com/vaplv/rsys/) and
[Star-VX](https://gitlab.com/meso-star/star-vx/) libraries, and on
[OpenMP](http://www.openmp.org) 1.2 to parallelize its computations.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the aforementioned prerequisites. Finally generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies. The
resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake documentation](https://cmake.org/documentation)
for further informations on CMake.

## Release notes

## License

Copyright (C) 2018, 2019, 2020, 2021 [|Meso|Star](http://www.meso-star.com)
(<contact@meso-star.com>). Copyright (C) 2018, 2019 Centre National de la
Recherche Scientifique (CNRS), Université Paul Sabatier
(<contact-edstar@laplace.univ-tlse.fr>). ECSky is free software released
under the GPL v3+ license: GNU GPL version 3 or later. You are welcome to
redistribute it under certain conditions; refer to the COPYING file for
details.

