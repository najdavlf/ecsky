/* Copyright (C) 2018, 2019, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ECSKY_H
#define ECSKY_H

#include <rsys/rsys.h>
#include <star/svx.h>
#include <limits.h> /* UINT_MAX support */

/* Library symbol management */
#if defined(ECSKY_SHARED_BUILD) /* Build shared library */
  #define ECSKY_API extern EXPORT_SYM
#elif defined(ECSKY_STATIC) /* Use/build static library */
  #define ECSKY_API extern LOCAL_SYM
#else /* Use shared library */
  #define ECSKY_API extern IMPORT_SYM
#endif

#ifndef NDEBUG
  #define ECSKY(Func) ASSERT(ecsky_ ## Func == RES_OK)
#else
  #define ECSKY(Func) ecsky_ ## Func
#endif

enum ecsky_property {
  ECSKY_Ks, /* Scattering coefficient */
  ECSKY_Ka, /* Absorption coefficient */
  ECSKY_Kext, /* Extinction coefficient = Ks + Ka */
  ECSKY_PROPS_COUNT__
};

/* List of sky components */
enum ecsky_component {
  ECSKY_CPNT_GAS,
  ECSKY_CPNT_S1,
  ECSKY_CPNT_S2,
  ECSKY_CPNT_S3,
  ECSKY_CPNT_S4,
  ECSKY_CPNT_S5,
  ECSKY_CPNT_S6,
  ECSKY_CPNT_S7,
  ECSKY_CPNT_S8,
  ECSKY_CPNT_S9,
  ECSKY_CPNT_S10,
  ECSKY_CPNTS_COUNT__
};

/* Component of the sky for which the properties are queried */
enum htrdr_sky_component_flag {
  ECSKY_CPNT_FLAG_GAS = BIT(ECSKY_CPNT_GAS),
  ECSKY_CPNT_FLAG_S1 = BIT(ECSKY_CPNT_S1),
  ECSKY_CPNT_FLAG_S2 = BIT(ECSKY_CPNT_S2),
  ECSKY_CPNT_FLAG_S3 = BIT(ECSKY_CPNT_S3),
  ECSKY_CPNT_FLAG_S4 = BIT(ECSKY_CPNT_S4),
  ECSKY_CPNT_FLAG_S5 = BIT(ECSKY_CPNT_S5),
  ECSKY_CPNT_FLAG_S6 = BIT(ECSKY_CPNT_S6),
  ECSKY_CPNT_FLAG_S7 = BIT(ECSKY_CPNT_S7),
  ECSKY_CPNT_FLAG_S8 = BIT(ECSKY_CPNT_S8),
  ECSKY_CPNT_FLAG_S9 = BIT(ECSKY_CPNT_S9),
  ECSKY_CPNT_FLAG_S10 = BIT(ECSKY_CPNT_S10),
  ECSKY_CPNT_MASK_ALL = ECSKY_CPNT_FLAG_GAS | ECSKY_CPNT_FLAG_S1 |
    ECSKY_CPNT_FLAG_S2 | ECSKY_CPNT_FLAG_S3 | ECSKY_CPNT_FLAG_S4 |
    ECSKY_CPNT_FLAG_S5 | ECSKY_CPNT_FLAG_S6 | ECSKY_CPNT_FLAG_S7 |
    ECSKY_CPNT_FLAG_S8 | ECSKY_CPNT_FLAG_S9 | ECSKY_CPNT_FLAG_S10

};

enum ecsky_svx_op {
  ECSKY_SVX_MIN,
  ECSKY_SVX_MAX,
  ECSKY_SVX_OPS_COUNT__
};

enum ecsky_spectral_type {
  ECSKY_SPECTRAL_LW, /* Longwave */
  ECSKY_SPECTRAL_SW, /* Shortwave */
  ECSKY_SPECTRAL_TYPES_COUNT__
};

struct ecsky_args {
  const char* ecrp_filename;
  const char* htgop_filename;
  const char* cache_filename; /* May be NULL <=> no cached data structure */
  const char* name; /* Name of the sky */
  enum ecsky_spectral_type spectral_type;
  double wlen_range[2]; /* Spectral range to handle. In nm */
  unsigned grid_max_definition[3]; /* Maximum definition of the grid */
  double optical_thickness; /* Threshold used during octree building */
  unsigned nthreads; /* Hint on the number of threads to use */
  int repeat_clouds; /* Define if the clouds are infinitely repeated in X and Y */
  int verbose; /* Verbosity level */
};

#define ECSKY_ARGS_DEFAULT__ {                                                 \
  NULL, /* ecrp_filename */                                                    \
  NULL, /* htgop_filename */                                                   \
  NULL, /* cache filename */                                                   \
  "sky", /* Name */                                                            \
  ECSKY_SPECTRAL_TYPES_COUNT__, /* spectral type */                            \
  {DBL_MAX,-DBL_MAX}, /* Spectral integration range */                         \
  {UINT_MAX, UINT_MAX, UINT_MAX}, /* Maximum definition of the grid */         \
  1, /* Optical thickness */                                                   \
  (unsigned)~0, /* #threads */                                                 \
  0, /* Repeat clouds */                                                       \
  0 /* Verbosity level */                                                      \
}
static const struct ecsky_args ECSKY_ARGS_DEFAULT = ECSKY_ARGS_DEFAULT__;

/* Forward declarations of external data types */
struct logger;
struct mem_allocator;
struct ssp_rng;
struct svx_voxel;

/* Opaque data type */
struct ecsky;

BEGIN_DECLS

/*******************************************************************************
 * HTSky API
 ******************************************************************************/
ECSKY_API res_T
ecsky_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const struct ecsky_args* args,
   struct ecsky** ecsky);

ECSKY_API res_T
ecsky_ref_get
  (struct ecsky* ecsky);

ECSKY_API res_T
ecsky_ref_put
  (struct ecsky* ecsky);

ECSKY_API const char*
ecsky_get_name
  (const struct ecsky* ecsky);

ECSKY_API double
ecsky_component_fetch_asymetry_parameter
  (const struct ecsky* sky,
   const size_t icomp,
   const double pos[3]);

ECSKY_API double
ecsky_component_fetch_raw_property
  (const struct ecsky* sky,
   const enum ecsky_property prop,
   const size_t icomp,
   const size_t ivox[3]);

ECSKY_API double
ecsky_fetch_raw_property
  (const struct ecsky* sky,
   const enum ecsky_property prop,
   const int components_mask, /* Combination of ecsky_component_flag */
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const double pos[3],
   /* For debug only. Assert if the fetched property is not in [k_min, k_max] */
   const double k_min,
   const double k_max);

ECSKY_API double
ecsky_fetch_temperature
  (const struct ecsky* sky,
   const double pos[3]);

ECSKY_API double
ecsky_fetch_svx_property
  (const struct ecsky* sky,
   const enum ecsky_property prop,
   const enum ecsky_svx_op op,
   const int components_mask, /* Combination of ecsky_component_flag */
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const double pos[3]);

ECSKY_API double
ecsky_fetch_svx_voxel_property
  (const struct ecsky* sky,
   const enum ecsky_property prop,
   const enum ecsky_svx_op op,
   const int components_mask,
   const size_t in_clouds,
   const size_t in_atmosphere,
   const size_t ispectral_band, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const struct svx_voxel* voxel);

ECSKY_API res_T
ecsky_trace_ray
  (struct ecsky* sky,
   const double org[3],
   const double dir[3], /* Must be normalized */
   const double range[2],
   const svx_hit_challenge_T challenge, /* NULL <=> Traversed up to the leaves */
   const svx_hit_filter_T filter, /* NULL <=> Stop RT at the 1st hit voxel */
   void* context, /* Data sent to the filter functor */
   const size_t ispectral_band,
   const size_t iquadrature_pt,
   struct svx_hit* hit);

ECSKY_API size_t
ecsky_get_spectral_bands_count
  (const struct ecsky* sky);

ECSKY_API size_t
ecsky_get_spectral_band_id
  (const struct ecsky* sky,
   const size_t i);

ECSKY_API size_t
ecsky_get_spectral_band_quadrature_length
  (const struct ecsky* sky,
   const size_t iband);

ECSKY_API res_T
ecsky_get_spectral_band_bounds
  (const struct ecsky* sky,
   const size_t iband,
   double wavelengths[2]);

/* Retrieve the spectral range of the loaded sky data overlapped by the user
 * defined wavelength range. */
ECSKY_API res_T
ecsky_get_raw_spectral_bounds
  (const struct ecsky* sky,
   double wavelengths[2]);

ECSKY_API enum ecsky_spectral_type
ecsky_get_spectral_type
  (const struct ecsky* sky);

ECSKY_API size_t
ecsky_get_ncomponents
  (const struct ecsky* sky);

/* Return the index of the band containing the submitted wavelength or SIZE_MAX
 * if `wavelength' is not included in any band */
ECSKY_API size_t
ecsky_find_spectral_band
  (const struct ecsky* sky,
   const double wavelength); /* In nanometer */

/* Return the sampled quadrature point */
ECSKY_API size_t
ecsky_spectral_band_sample_quadrature
  (const struct ecsky* sky,
   const double r, /* Random number in [0, 1[ */
   const size_t ispectral_band);

ECSKY_API res_T
ecsky_dump_cloud_vtk
  (const struct ecsky* sky,
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point */
   FILE* stream);

ECSKY_API enum htrdr_sky_component_flag
ecsky_mask_all_components
  (const struct ecsky* sky);

END_DECLS

#endif /* ECSKY_H */
